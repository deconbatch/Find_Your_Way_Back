/**
 * Find Your Way Back.
 * explore the game world!
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.5.3
 * 2020.09.20
 */

void setup() {
  size(720, 640);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();
  noLoop();
}

void draw() {

  int frmMax  = 24 * 12; // 24fps x 12sec
  int nodeMax = 3;
  int nodeDiv = 4;
  float nStep = 0.006; // noise pattern step
  float wStep = 0.5;   // wandering step

  translate(width * 0.5, height * 0.5);

  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {

    float frmRatio = map(frmCnt, 0, frmMax, 0.0, 1.0);
    noStroke();

    // draw focusing to the center
    for (int i = 0; i <= 20; i++) {
      float ratio = map(i, 0, 20, 1.0, 0.0);
      fill(0, 0, 30 * ratio, 100);
      ellipse(0.0, 0.0, width * (0.5 + ratio), height * (0.5 + ratio));
    }

    // draw noise field
    for (float nX = -width * 0.5; nX <= width * 0.5; nX += nodeDiv * nodeMax) {
      for (float nY = -height * 0.5; nY <= height * 0.5; nY += nodeDiv * nodeMax) {
        float x = nX;
        float y = nY;
        for (int nodeCnt = 0; nodeCnt < nodeMax; nodeCnt++) {
          // wandering about
          float wander = noise(
                              wStep * (2.0 + cos(TWO_PI * frmRatio)),
                              wStep * (2.0 + sin(TWO_PI * frmRatio))
                               ) * width * 0.5;
          // noise pattern + wandering
          float nVal = noise(
                             (width + nX + wander * cos(frmRatio * TWO_PI)) * nStep,
                             (height + nY + wander * sin(frmRatio * TWO_PI)) * nStep
                             );
          // limit 8 directions
          float theta = floor(16.0 * nVal) * TWO_PI / 8.0;
          theta %= TWO_PI;
          x += nodeDiv * cos(theta);
          y += nodeDiv * sin(theta);
          fill((theta / TWO_PI) * 360.0, 90.0, 80.0, 100.0);
          ellipse(x, y, nodeDiv, nodeDiv);
        }
      }
    }
    
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");

  }
  exit();
}
